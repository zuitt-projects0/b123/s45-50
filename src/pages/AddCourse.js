import React,{useState,useEffect,useContext} from 'react';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import {Redirect,useHistory} from 'react-router-dom';

import UserContext from '../userContext';

export default function AddCourse(){

	//get our global user state from our context
	const {user} = useContext(UserContext);

	const history = useHistory();

	//input states
	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState(0);

	//state for our submit button conditional rendering
	const [isActive,setIsActive] = useState(true);

	//useEffect to check input and disable/enable the submit button
	useEffect(()=>{

		if(name !== "" && description !== "" && price !== 0){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name, description,price]);

	/*get token from localStorage*/
/*	console.log(localStorage.getItem('token'))
	console.log(localStorage.token)*/
	
	function createCourse(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('http://localhost:4000/courses/',{

			method: 'POST',
			headers: {
				//if your request has a body, you have to pass the content type header.
				'Content-Type': 'application/json',
				//if your request needs a token, you have to pass authorization header.
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message){
				Swal.fire({

					icon: "error",
					title: "Course Creation Failed.",
					text: data.message

				})
				
			} else {
				console.log(data)
				Swal.fire({

					icon: "success",
					title: "Course Creation Successful.",
					text: `Course has been created.`

				})
				history.push("/courses");
			}

		})


		setName("");
		setDescription("");
		setPrice(0);

	};

	/*added ternary to conditionally render the form. Redirect the user if he is regular or a guest (not logged in)*/
	return (
		user.isAdmin !== true
		?
		<Redirect to="/"/>
		:
		<>
			<h1>Create Course</h1>
			<Form onSubmit={ e => createCourse(e)}>
				<Form.Group controlId="name">
					<Form.Label>Course Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type="number"
						placeholder="Enter Price"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive 
					? <Button type="submit" variant="primary">Submit</Button>
					: <Button type="submit" variant="danger" disabled>Submit</Button>
				}
			</Form>
		</>

		)
}
