import React from 'react'
import Banner from '../components/Banner'


export default function NotFound(){

	let bannerContent={

		title: 'Page not Found',
		description: "Book your favorite course!",
		buttonCallToAction: "Back to Home",
		destination: "/"
	}

	return (


			<Banner bannerProp={bannerContent}/>


		)

}
