import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext'
import Banner from '../components/Banner'

export default function Logout(){

	//destructure the returned object of useContext after unwrapping our context:
	const {setUser,unsetUser} = useContext(UserContext);

/*	console.log(setUser);
	console.log(unsetUser);*/

	//Clear the localStorage
	unsetUser();

	//add empty dependency array to run the useEffect only on initial render.
	useEffect(()=>{

		//Can you update a state included in the context with its setter function? Yes.
		//set the global user state to its initial values.
		setUser({

			id:null,
			isAdmin:null

		})	

	},[])

	const bannerContent = {

		title: "See You Later!",
		description: "You have logged out of B123 Booking System",
		buttonCallToAction: "Go Back To Home Page.",
		destination: "/"

	}

	return (

			<Banner bannerProp={bannerContent}/>


		)

}
