import React from  'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
/*
	Home will be a page component, which will be our pages for our application.

	ReactJS adheres to D.R.Y. - Don't Repeat Yourself.

	Props - are data we can pass from a parent component to child component.

	All components actually are able to receive an object, Props are special react objects with which we can pass data around from a parent to child.
*/
export default function Home(){

	//can we pass props fromm Home to Banner?
	let sampleProp = {
		title: "Batch 123 Booking System",
		description: "Book your favorite course!",
		buttonCallToAction: "Join Us!",
		destination: "/login"
	};


	/*
		We can pass props from a parent to child by adding HTML-like attribute which we can name ourselves. The name of the attribute will become the property of the object received by all components.

		Note: Components are indepedent from each other.

	*/
	return (

			<>
				<Banner	bannerProp={sampleProp}/>
				<Highlights />
			</>

		)

}
