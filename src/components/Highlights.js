import React from 'react';
import {Row,Col,Card} from 'react-bootstrap'

export default function Highlights(){

	return (

				<Row>
					<Col xs={12} md={4}>
						<Card className="cardHighlights">
							<Card.Body>
								<Card.Title>
									<h2>Learn from Home</h2>
								</Card.Title>
								<Card.Text>
									Ullamco irure officia laborum labore irure aliqua do elit in cillum duis non ex laboris labore sed voluptate proident.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={4}>
						<Card className="cardHighlights">
							<Card.Body>
								<Card.Title>
									<h2>Study Now, Pay Later</h2>
								</Card.Title>
								<Card.Text>
									Veniam elit id incididunt esse ut aliqua ex enim proident tempor ea occaecat ullamco adipisicing sed laborum non sit consequat veniam minim ex sit anim deserunt tempor consequat qui dolor aute esse mollit voluptate in velit.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={4}>
						<Card className="cardHighlights">
							<Card.Body>
								<Card.Title>
									<h2>Be Part of Our Community</h2>
								</Card.Title>
								<Card.Text>
									Laboris occaecat culpa ea nostrud proident id nulla amet cupidatat in laborum. Officia culpa consequat minim aute do ad et id duis consequat ut amet eiusmod cupidatat.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>
				</Row>
		)

}